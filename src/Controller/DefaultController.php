<?php

namespace Acme\Application\Controller;

use Silex\Application;

class DefaultController
{
    public function index(Application $app)
    {
        return $app['twig']->render('/app/views/index.html.twig');
    }
}
