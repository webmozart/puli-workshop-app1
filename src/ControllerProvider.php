<?php

namespace Acme\Application;

use Silex\Application;
use Silex\ControllerProviderInterface;

class ControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'Acme\\Application\\Controller\\DefaultController::index');

        return $controllers;
    }
}
