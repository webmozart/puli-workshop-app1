gulp.task('css', function () {
    gulp.src(['node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss', 'res/scss/style.scss'])
        .pipe(concat('style.scss'))
        .pipe(sass())
        .pipe(gulp.dest('res/public/css/'));
});
