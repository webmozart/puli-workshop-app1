gulp.task('js', function () {
    gulp.src([
            'res/js/script.js',
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js'
        ])
        .pipe(gulp.dest('res/public/js/'));
});
